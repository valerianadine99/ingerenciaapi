package com.pucp.beer.service;

import com.pucp.beer.dto.Beer;
import com.pucp.beer.dto.SearchLog;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Service
public class BeerService {
    private List<Beer> beerList = new ArrayList<>();
    private String punkApi = "https://api.punkapi.com/v2/";

    public List<Beer> getAllBeers(String beerName){
        String uri= punkApi + "beers";

        if (beerName != null){
            uri += "?beer_name=" + beerName;
        }

        RestTemplate restTemplate = new RestTemplate();
        Beer[] beers = restTemplate.getForObject(uri, Beer[].class);
        beerList = Arrays.asList(beers);

        return beerList;
    }

    public List<Beer> getRandomBeer(){
        String uri= punkApi + "beers/random";
        RestTemplate restTemplate = new RestTemplate();

        Beer[] randomBeers = restTemplate.getForObject(uri, Beer[].class);
        beerList = Arrays.asList(randomBeers);

        return beerList;
    }
}

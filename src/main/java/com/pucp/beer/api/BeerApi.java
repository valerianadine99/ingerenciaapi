package com.pucp.beer.api;

import com.pucp.beer.dto.Beer;
import com.pucp.beer.dto.SearchLog;
import com.pucp.beer.service.BeerService;
import com.pucp.beer.service.SearchLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import java.net.http.HttpRequest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@RestController
public class BeerApi {

    @Autowired
    private SearchLogService searchLogService;
    @Autowired
    private BeerService beerService;

    @RequestMapping(value="/beers", method = RequestMethod.GET)
    public List<Beer> getBeers(@RequestParam(required = false) String beerName, HttpServletRequest request){
        long startTime = System.currentTimeMillis();

        var result = beerService.getAllBeers(beerName);

        long timeTaken = System.currentTimeMillis() - startTime;
        searchLogService.save(new SearchLog(request.getServletPath(), new Date(), String.valueOf(timeTaken)));
        return result;
    }

    @RequestMapping(value="/beers/random", method = RequestMethod.GET)
    public List<Beer> getRandomBeer(HttpServletRequest request){
        long startTime = System.currentTimeMillis();

        var result = beerService.getRandomBeer();

        long timeTaken = System.currentTimeMillis() - startTime;
        searchLogService.save(new SearchLog(request.getServletPath(), new Date(), String.valueOf(timeTaken)));
        return result;
    }
}

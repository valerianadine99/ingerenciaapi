package com.pucp.beer.dto;

public class Beer {
    String id;
    String name;
    String tagline;
    String first_brewed;
    String description;
    String image_url;

    public Beer() {
    }

    public Beer(String id, String name, String tagline, String first_brewed, String description, String image_url) {
        this.id = id;
        this.name = name;
        this.tagline = tagline;
        this.first_brewed = first_brewed;
        this.description = description;
        this.image_url = image_url;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getTagline() {
        return tagline;
    }

    public String getFirst_brewed() {
        return first_brewed;
    }

    public String getDescription() {
        return description;
    }

    public String getImage_url() {
        return image_url;
    }
}
